/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author catalinaaguayo
 */
public class Persona {
    
    private String Nombre;
    private String Telefono;
    private String Correo;
    private String Direccion;
    
    public Persona (String n, String t, String c, String d){
        this.Nombre=n;
        this.Telefono=t;
        this.Correo=c;
        this.Direccion=d;
    }
    
    public boolean validarNombre(String palabra){
    Pattern pat = Pattern.compile("([A-Z]{1}[a-z]{1,39}[\\s]*)+");
    Matcher mat = pat.matcher(palabra);
        return mat.matches();
    }
    
    public boolean validarTelefono (String telefono){
    Pattern pat = Pattern.compile("^[+]{1}[\\d]*{7,15}");
    Matcher mat = pat.matcher(telefono);
        return mat.matches();    
    }
    
    public boolean validarCorreo (String correo){
    Pattern pat = Pattern.compile("[a-zA-Z1-9]{2,64}[@]{1}[a-z]*[.]{1}[a-z]{2,255}");
    Matcher mat = pat.matcher(correo);
        return mat.matches();    
    }
        
    public boolean validarDireccion (String direccion){
    Pattern pat = Pattern.compile("([A-Z]{1}[a-z]{1,39}[\\s]*)+[\\d]{1,6}");
    Matcher mat = pat.matcher(direccion);
        return mat.matches();    
    }
    
}
