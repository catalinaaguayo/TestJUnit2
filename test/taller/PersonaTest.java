/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author catalinaaguayo
 */
public class PersonaTest {
    
    Persona p = new Persona("Catalina Aguayo","+56984977000", "capagmu@gmail.com", "Gabriela Mistral 0890");
    
    public PersonaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validarNombre method, of class Persona.
     */
    @Test
    public void testValidarNombre() {
        assertEquals(true, p.validarNombre("Catalina Aguayo"));
    }

    
    /**
     * Test of validarTelefono method, of class Persona.
     */
    @Test
    public void testValidarTelefono() {
        
        assertEquals(true, p.validarTelefono("+56998989898"));
        
    }

    /**
     * Test of validarCorreo method, of class Persona.
     */
    @Test
    public void testValidarCorreo() {
        assertEquals(true, p.validarCorreo("capagmu1@gmail.com"));
    }

    /**
     * Test of validarDireccion method, of class Persona.
     */
    @Test
    public void testValidarDireccion() {
        assertEquals(true, p.validarDireccion("Gabriela Mistral 0890"));
    }
    
}
